﻿Imports System.Diagnostics
Imports System.IO
Imports System.Text
Imports System.Threading

Public Class Form1

    Public Shared TextB As TextBox
    Public _script As Script

    Sub FormLoaded(s, e) Handles MyBase.Load
        Go.LoadActions()
    End Sub
    Sub Form_Closed(s, e) Handles MyBase.FormClosed
        Application.Exit()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        _script = Script.ParseFile(File.Open("script.vb", FileMode.OpenOrCreate))

        TextBox1.Text = String.Empty

        WriteErrors()
        WriteScript(_script)

        Button2.Enabled = Script.Errors.Count = 0
    End Sub

    Sub WriteErrors()
        For Each Err As KeyValuePair(Of UInt16, String) In Script.Errors
            TextBox1.Text &= Chr(10) & Chr(13) & "Line: " & Err.Key.ToString().PadRight(5) & Chr(32) & Err.Value & Chr(10) & Chr(13)
        Next
        If Script.Errors.Count > 0 Then
            TextBox1.Text &= Chr(10) & Chr(13) & Chr(10) & Chr(13)
        End If
    End Sub
    Sub WriteScript(S As Script, Optional depth As UInt16 = 0)
        TextBox1.Text &= Chr(13) & Chr(10) & "".PadLeft(depth * 2, " ") & "void " & S.Name & "() {" & Chr(13) & Chr(10)

        For Each X As Script In S.Stack.Values
            WriteScript(X, depth + 1)
        Next

        For Each C As Command In S
            TextBox1.Text &= "".PadLeft(depth * 2, " ") & "  " & C.ToString() & ";" & Chr(13) & Chr(10)
        Next

        TextBox1.Text &= "".PadLeft(depth * 2, " ") & "}" & Chr(13) & Chr(10) & Chr(13) & Chr(10)
    End Sub

    Private Sub Button2_Click(s, e) Handles Button2.Click
        TextBox1.Text = String.Empty
        TextB = TextBox1
        TextBox1.Refresh()

        Script.Root.Execute()
    End Sub

    Private Sub Button3_Click(s, e) Handles Button3.Click
        If npp_id > -1 Then Exit Sub

        Dim npp_to_run$ = String.Empty
        Dim paths As String() = {
            Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\Notepad++\notepad++.exe",
            Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & " (x86)\Notepad++\notepad++.exe",
            "notepad"
            }

        For I As Byte = 0 To paths.Length - 1
            npp_to_run = paths(I)
            If File.Exists(npp_to_run) Then Exit For
        Next

        Dim thread As New Thread(Sub()
                                     Dim pid As Integer = Shell(npp_to_run & " " & Chr(34) & Application.StartupPath & "\script.vb" & Chr(34))

                                     Button3.Invoke(Sub()
                                                        Button3.Enabled = False
                                                    End Sub)

                                     Dim proc As Process = New Process()
                                     Try
                                         proc = Process.GetProcessById(pid)
                                         SetActiveWindow(proc.MainWindowHandle)
                                     Catch : Exit Sub
                                     End Try

                                     npp_id = pid
                                     proc.WaitForExit()
                                     npp_id = -1

                                     Button3.Invoke(Sub()
                                                        Button3.Enabled = True
                                                    End Sub)

                                 End Sub)
        thread.Start()
    End Sub

    Dim npp_id As Integer = -1
    Private Declare Function SetActiveWindow Lib "user32.dll" (HWND As IntPtr) As IntPtr

End Class