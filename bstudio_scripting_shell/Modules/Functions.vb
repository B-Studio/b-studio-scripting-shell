﻿Imports System.Text.RegularExpressions

Public Module Functions

    ''' <summary> trim, toLower </summary>
    Function Norm(Str$) As String
        Return Str.Trim().ToLower()
    End Function

    Function RegFindAll(Haystack$, Regex$) As String()
        Dim rx As New Regex(Regex, RegexOptions.IgnoreCase Or RegexOptions.Singleline)
        Dim m As MatchCollection = rx.Matches(Haystack)
        Dim res As New List(Of String)

        For Each x As Match In m
            For i As Int32 = 1 To x.Groups.Count
                Dim xx$ = x.Groups(i).Value
                If xx.Length = 0 OrElse xx = Haystack Then Continue For
                res.Add(xx)
            Next
        Next


        Return res.ToArray()
    End Function

    Function RegReplace(SrcStr$, RegFind$, RepStr$) As String
        Dim R As New Regex(RegFind)
        Return R.Replace(SrcStr, RepStr)
    End Function
    Function RegFind(Haystack$, RegStr$) As Boolean
        Dim R As New Regex(RegStr, RegexOptions.IgnoreCase)
        Return R.IsMatch(Haystack)
    End Function
    Function NotRegFind(Haystack$, RegStr$) As Boolean
        Return Not RegFind(Haystack, RegStr)
    End Function

    Sub RemoveDoubleSpaces(ByRef Str$)
        Str = RegReplace(Str$, "\s+", Chr(32))
    End Sub

    Function InArray(Needle, Haystack()) As Boolean
        Return Array.IndexOf(Haystack, Needle) > -1
    End Function

End Module