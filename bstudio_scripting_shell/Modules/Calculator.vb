﻿Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Collections.Generic
Module Calculator

    Private op_priorities As New List(Of String) From {"^", "*\%", "+_", "><=!", "|&"}
    Private Delegate Function RegChecker(Haystack$, RegExp$) As Boolean
    Private Validators As New Dictionary(Of String, RegChecker) From {
        {"[A-Za-z_]+", AddressOf NotRegFind},
        {"([^)\d]+(\)|$)|\(\))", AddressOf NotRegFind},
        {"([\\><*%+^!-]{2,}|[^)\d]+$|^[^(\d+-]+)", AddressOf NotRegFind},
        {"\\0+((?![,.])[^\d]|$)", AddressOf NotRegFind},
        {"([\.,]\d*[\.,])+", AddressOf NotRegFind}
    }
    Private ValidatorDescrips As New Dictionary(Of Byte, String) From {
        {0, "Not all variables are resolved"},
        {1, "Syntax error in brackets hierarchy"},
        {2, "Syntax error in order of operators"},
        {3, "Division by a zero"},
        {4, "Error on read a decimal number (excess dilimiter?)"},
        {Byte.MaxValue, "Empty expression is given"}
    }

    Public Sub SolveVariables(ByRef Expression As String, Vars As Dictionary(Of String, Double), Optional isTest As Boolean = False)
        Expression = Expression.Replace("."c, ","c).Replace("/", "\").Replace(" ", String.Empty)
        PrepareDoubleSigned(Expression)

        Dim cur, pre, nex As Char, p$ = String.Empty
        For x% = 0 To Expression.Length - 1
            cur = Expression(x)
            If x > 0 Then pre = Expression(x - 1) Else pre = "$"c
            If x < Expression.Length - 1 Then nex = Expression(x + 1) Else nex = "$"c

            If cur = "("c AndAlso (pre = ")"c OrElse IsNumeric(pre)) Then
                p &= "*("
            ElseIf cur = ")"c AndAlso (nex = "("c OrElse IsNumeric(nex)) Then
                p &= ")*"
            Else : p &= Expression(x)
            End If
        Next
        Expression = p

        If isTest Then
            Expression = RegReplace(Expression, "([A-Za-z_][A-Za-z\d_]*)", 1)
            Exit Sub
        End If

        Dim regFinder$ = "("
        For Each K As String In Vars.Keys : regFinder &= K & "|" : Next
        regFinder = regFinder.TrimEnd("|"c) & ")"


        For Each var$ In RegFindAll(Expression, regFinder)
            Expression = Expression.Replace(var, Vars(var))
        Next
    End Sub
    Public Function IsValidExpr(Expression$) As String
        If Expression.Trim().Length = 0 Then Return ValidatorDescrips(Byte.MaxValue)

        Dim o = 0
        For Each c As Char In Expression
            If c = "("c Then
                o += 1
            ElseIf c = ")"c Then
                If o = 0 Then Return ValidatorDescrips(1)
                o -= 1
            End If
        Next
        If o > 0 Then Return ValidatorDescrips(1)

        Dim i As Byte = 0
        For Each validator As KeyValuePair(Of String, RegChecker) In Validators
            If Not validator.Value(Expression, validator.Key) Then
                Return ValidatorDescrips(i)
            End If
            i += 1
        Next

        Return String.Empty
    End Function


    Function Eval(Expression As String, ByRef Variables As Dictionary(Of String, Double)) As Double
        Expression = Expression.Replace("."c, ","c).Replace("/", "\")
        PrepareDoubleSigned(Expression)

        Dim logicOps$ = "&|="
        If Expression.IndexOfAny(logicOps) > -1 Then
            For Each c As Char In logicOps
                Expression = RegReplace(Expression, "\" & c & "+", c)
            Next
        End If

        For Each var$ In Variables.Keys
            Expression = Expression.Replace(var, Variables(var))
        Next
        Expression = Expression.Replace(Chr(32), String.Empty)


        Dim cur, pre, nex As Char, p$ = String.Empty
        For x% = 0 To Expression.Length - 1
            cur = Expression(x)
            If x > 0 Then pre = Expression(x - 1) Else pre = "$"c
            If x < Expression.Length - 1 Then nex = Expression(x + 1) Else nex = "$"c

            If cur = "("c AndAlso (pre = ")"c OrElse IsNumeric(pre)) Then
                p &= "*("
            ElseIf cur = ")"c AndAlso (nex = "("c OrElse IsNumeric(nex)) Then
                p &= ")*"
            Else : p &= Expression(x)
            End If
        Next
        Expression = p

        Dim err_of_invalid = IsValidExpr(Expression)
        If err_of_invalid <> String.Empty Then
            Throw New InvalidExpressionException(err_of_invalid & ". Solved on last step: " & Expression)
        End If

        Dim findDeepest = Function(exp) As Integer()
                              Dim openPos% = -1,
                                  closePos% = -1,
                                  currDepth% = 0,
                                  maxDepth% = 0

                              For i% = 0 To exp.Length - 1
                                  If exp(i) = "(" Then
                                      currDepth += 1
                                      If currDepth > maxDepth Then
                                          maxDepth = currDepth
                                          openPos = i
                                          closePos = -1
                                      End If
                                  ElseIf exp(i) = ")" Then
                                      If currDepth = maxDepth AndAlso closePos = -1 Then
                                          closePos = i
                                      End If
                                      currDepth -= 1
                                  End If
                              Next

                              Return {openPos, closePos}
                          End Function

        Dim subexp$ = String.Empty
        Dim pos As Int32() = findDeepest(Expression)
        Dim calc As Double = 0

        While pos(1) > -1
            subexp = Expression.Substring(pos(0) + 1, pos(1) - pos(0) - 1)
            calc = calcExpr(subexp)

            Expression = Expression.Remove(pos(0)) & calc & Expression.Remove(0, pos(1) + 1)

            pos = findDeepest(Expression)
        End While

        Return calcExpr(Expression)
    End Function

    Sub PrepareBoolExpression(ByRef Exp$)
        Dim ops$ = ">!=<"

        Dim se_cache$ = ""
        Dim outp = ""
        Dim explen As UInt32 = Exp.Length
        Dim x As UInt32 = 0

        While explen > x AndAlso ops.IndexOf(Exp(x)) = -1
            se_cache &= Exp(x)
            x += 1
        End While
        outp &= se_cache

        If x < explen Then
            outp &= Exp(x)
            x += 1
        End If
        se_cache = ""


        For i As UInt32 = x To explen - 1
            While explen > i AndAlso ops.IndexOf(Exp(i)) = -1
                se_cache &= Exp(i)
                i += 1
            End While
            outp &= se_cache

            If i < explen Then
                outp &= "&" & se_cache & Exp(i)
            End If

            se_cache = ""
        Next

        Exp = outp
    End Sub

    Sub PrepareDoubleSigned(ByRef Exp$)
        While RegFind(Exp, "-{2,}")
            Exp = RegReplace(Exp, "-{2}", "+")
        End While
        Exp = RegReplace(Exp, "\+{2,}", "+")
    End Sub

    Private Function calcExpr(Line$) As Double
        currentExp$ = Line

        Dim p$ = String.Empty
        For x% = 0 To Line.Length - 1
            If x = 0 AndAlso Line(x) = "+" Then : Continue For
            ElseIf x > 0 AndAlso Line(x) = "-"c AndAlso IsNumeric(Line(x - 1)) Then
                p &= "_"
            ElseIf Line(x) = "!"c AndAlso x + 1 < Line.Length AndAlso Line(x + 1) = "="c Then
                p &= "!"
                x += 1
            Else : p &= Line(x)
            End If
        Next
        Line = p

        If Line.IndexOfAny("=<>!") > -1 Then PrepareBoolExpression(Line)

        Dim ops As New List(Of Char)
        ops.AddRange(RegReplace(Line, "(-?\d+(?:[\.,]\d+)*)", String.Empty))

        If ops.Count = 0 Then
            Return Double.Parse(Line)
        End If

        Dim N As New List(Of Double)
        Dim m As String() = RegFindAll(Line, "(-?\d+(?:[\.,]\d+)*)")
        For Each num As String In m
            N.Add(Double.Parse(num))
        Next

        For Each operators_set$ In op_priorities
            Dim i As UInt32 = 0,
                smth_found As Boolean = False

            Do While i < ops.Count

                For Each op$ In operators_set
                    If (ops(i) = op) Then
                        N(i) = calcs(op)(N(i), N(i + 1))
                        N.RemoveAt(i + 1)
                        ops.RemoveAt(i)
                        If ops.Count = 0 Then Return N(0)
                        smth_found = True
                        Continue Do
                    End If
                Next

                i += 1
            Loop
        Next


        Return N(0)
    End Function

    Private currentExp$ = "- -"
    Private Delegate Function calcor(n1#, n2#) As Double
    Private calcs As New Dictionary(Of String, calcor) From {
        {
            "^",
            Function(n1#, n2#) As Double
                If n1 < 0 AndAlso n2 < 1 AndAlso n2 > 0 Then
                    Throw New ArgumentException("Root of negative number in expression: " & currentExp)
                End If
                Return Math.Pow(n1, n2)
            End Function
        },
        {
            "*",
            Function(n1#, n2#) As Double
                Return n1 * n2
            End Function
        },
        {
            "\",
            Function(n1#, n2#) As Double
                If n2 = 0 Then Throw New ArgumentException("Division by zero in expression: " & currentExp)
                Return n1 / n2
            End Function
        },
        {
            "%",
            Function(n1#, n2#) As Double
                If n2 = 0 Then Throw New ArgumentException("Division by zero in expression: " & currentExp)
                Return n1 Mod n2
            End Function
        },
        {
            "+",
            Function(n1#, n2#) As Double
                Return n1 + n2
            End Function
        },
        {
            "_",
            Function(n1#, n2#) As Double
                Return n1 - n2
            End Function
        },
        {
            "<",
            Function(n1#, n2#) As Double
                If n1 < n2 Then Return 1 Else Return 0
            End Function
        },
        {
            ">",
            Function(n1#, n2#) As Double
                If n1 > n2 Then Return 1 Else Return 0
            End Function
        },
        {
            "=",
            Function(n1#, n2#) As Double
                If n1 = n2 Then Return 1 Else Return 0
            End Function
        },
        {
            "!",
            Function(n1#, n2#) As Double
                If n1 = n2 Then Return 0 Else Return 1
            End Function
        },
        {
            "|",
            Function(n1#, n2#) As Double
                If n1 <> 0 OrElse n2 <> 0 Then Return 1 Else Return 0
            End Function
        },
        {
            "&",
            Function(n1#, n2#) As Double
                If n1 = 0 OrElse n2 = 0 Then Return 0 Else Return 1
            End Function
        }
    }


End Module