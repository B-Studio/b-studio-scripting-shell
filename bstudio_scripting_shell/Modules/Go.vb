﻿Imports System.Reflection

Public Class Go
#Region "SCENARIO FUNCTIONS"

    Public Shared Actions As New Dictionary(Of String, MethodInfo)
    Public Shared Sub LoadActions()
        Dim Methods As MethodInfo() = (New Action).GetType().GetMethods()
        For Each M As MethodInfo In Methods
            If M.IsStatic Then Actions.Add(M.Name, M)
        Next
    End Sub



#Region "WINAPI"
    Private Declare Function mouse_event Lib "user32.dll" (ByVal dwFlags As Int32, ByVal dX As Int32, ByVal dY As Int32, ByVal cButtons As Int32, ByVal dwExtraInfo As Int32) As Boolean
    Private Declare Sub keybd_event Lib "user32.dll" (ByVal vKey As Byte, ByVal vScan As Byte, ByVal flags As Integer, ByVal extrainfo As Integer)
    Private Declare Function SetCursorPos Lib "user32.dll" (x As Int32, y As Int32) As Boolean
#End Region

    Public Shared Sub ScriptStarted()
        Go.Action.StartCurPoint = New Point(
            Cursor.Position.X,
            Cursor.Position.Y)
    End Sub

    Public Class Action
        Public Shared Sub [call](ByVal address As String)
            Dim S As Script = Script.FindByAddress(address)
            If S IsNot Nothing Then S.Execute()
        End Sub
        Public Shared Sub delay(msec)
            System.Threading.Thread.Sleep(CInt(msec))
        End Sub
        Public Shared Sub keypress(key)
            keybd_event(CByte(key), 0, 0, 0)
            System.Threading.Thread.Sleep(10)
            keybd_event(CByte(key), 0, 2, 0)
        End Sub
        Public Shared Sub keydown(key)
            keybd_event(CByte(key), 0, 0, 0)
        End Sub
        Public Shared Sub keyup(key)
            keybd_event(CByte(key), 0, 2, 0)
        End Sub
        Public Shared Sub lclick()
            mouse_event(2, 0, 0, 0, 0)
            System.Threading.Thread.Sleep(10)
            mouse_event(4, 0, 0, 0, 0)
        End Sub
        Public Shared Sub rclick()
            mouse_event(8, 0, 0, 0, 0)
            System.Threading.Thread.Sleep(10)
            mouse_event(16, 0, 0, 0, 0)
        End Sub
        Public Shared Sub mclick()
            mouse_event(32, 0, 0, 0, 0)
            System.Threading.Thread.Sleep(10)
            mouse_event(64, 0, 0, 0, 0)
        End Sub
        Public Shared Sub scroll(pts)
            Dim sgn As SByte = Math.Sign(CSByte(pts))
            For a As SByte = 0 To CSByte(pts) Step sgn
                mouse_event(&H800, 0, 0, sgn * 120, 0)
                System.Threading.Thread.Sleep(10)
            Next
        End Sub
        Public Shared Sub move(x, y)
            SetCursorPos(CUShort(x), CUShort(y))
        End Sub
        Public Shared Sub click(x, y)
            move(x, y)
            lclick()
        End Sub
        Public Shared Sub write(string$)
            SendKeys.SendWait([string])
        End Sub
        Public Shared StartCurPoint As Point
        Public Shared Sub retstart()
            SetCursorPos(StartCurPoint.X, StartCurPoint.Y)
        End Sub
    End Class

#End Region
End Class