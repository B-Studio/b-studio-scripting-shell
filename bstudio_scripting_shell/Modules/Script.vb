﻿Imports System.IO
Imports System.Threading

Public Class Script
    Implements IEnumerable(Of Command)

    Public Const RootName As String = "(root)"

    Public Shared Property Root As Script
    Public Shared Errors As New Dictionary(Of UInt16, String)

    Private Shared Runtime As Thread

    Private Property Commands As List(Of Command)

    Public Property Name As String = RootName
    Public Property ParentScript As Script

    Public Property Stack As New Dictionary(Of String, Script)
    Public Property Variables As New Dictionary(Of String, Double)

    Public ReadOnly Property ScriptAddress As String
        Get
            Return Script.GetScriptAddress(Me).ToLower()
        End Get
    End Property

    Private Shared Function GetScriptAddress(S As Script) As String
        Dim curr As Script = S
        Dim addr As String = String.Empty

        While curr.ParentScript IsNot Nothing
            addr = curr.Name & " " & addr
            curr = curr.ParentScript
        End While

        addr = RootName & " " & addr

        Return addr.Trim()
    End Function
    Public Shared Function FindByName(ScriptName As String, Scope As Script) As Script
        For Each n As String In Scope.Stack.Keys
            If n = ScriptName Then Return Scope.Stack.Item(ScriptName)
        Next
        If Scope.ParentScript IsNot Nothing Then _
            Return FindByName(ScriptName, Scope.ParentScript)

        Return Nothing
    End Function
    Public Shared Function FindByAddress(Address As String) As Script
        Dim AdressPieces As String() = Address.ToLower().Split(" ")
        Dim curr As Script = Script.Root

        For Each Ar$ In AdressPieces
            If Ar = RootName Then Continue For
            If curr.Stack.ContainsKey(Ar) Then
                curr = curr.Stack.Item(Ar)
            Else
                curr = Nothing
                Exit For
            End If
        Next
        Return curr
    End Function

    Public Shared Function ParseFile(FromFile As Stream) As Script
        Dim RetScript As New Script
        Dim currentScript As Script = RetScript
        Script.Errors.Clear()

        Using sr As New StreamReader(FromFile)
            Dim lineCounter As UInt32 = 0
            While Not sr.EndOfStream
                lineCounter += 1
                Dim line$ = sr.ReadLine(),
                    nrm$ = Norm(line)

                If RegFind(nrm, "^function\s+[A-Za-z\d_-]+\s?.*$") Then
                    Dim scriptName$ = nrm.Split(" "c)(1)
                    If scriptName = RootName Then
                        Script.Errors.Add(lineCounter, "Function name " & RootName & " is reserved and cant be used as custom func-name")
                        Continue While
                    End If
                    Dim newScript As New Script(scriptName)
                    If currentScript.Stack.ContainsKey(scriptName) Then
                        currentScript.Stack.Remove(scriptName)
                    End If
                    GC.Collect()
                    currentScript.Stack.Add(scriptName, newScript)
                    newScript.ParentScript = currentScript
                    currentScript = newScript
                ElseIf RegFind(nrm, "^end\s+function.*$") Then
                    If currentScript.ParentScript IsNot Nothing Then _
                       currentScript = currentScript.ParentScript _
                    Else Script.Errors.Add(lineCounter, "Incorrect 'end-function' statement") 
                Else
                    Dim c As New Command(line)

                    Select Case c.Id
                        Case Command.Ids.empty
                            Continue While
                        Case Command.Ids.variable
                            If Not currentScript.Variables.ContainsKey(c.Arguments(0)) Then
                                currentScript.Variables.Add(c.Arguments(0), 0)
                            End If
                            If c.Arguments.Count > 1 Then
                                Dim expr$ = c.Arguments(1)
                                Calculator.SolveVariables(Expression:=expr, Vars:=currentScript.Variables, isTest:=True)

                                Dim invalid_msg$ = Calculator.IsValidExpr(expr)
                                If Not invalid_msg = String.Empty Then
                                    Script.Errors.Add(lineCounter, "Incorrect math expression. " & invalid_msg & " : " & c.Arguments(1))
                                    Continue While
                                End If
                            End If
                        Case Command.Ids.if
                            If c.Arguments.Count = 1 Then
                                Dim expr$ = c.Arguments(0)
                                Calculator.SolveVariables(Expression:=expr, Vars:=currentScript.Variables, isTest:=True)

                                Dim invalid_msg$ = Calculator.IsValidExpr(expr)
                                If Not invalid_msg = String.Empty Then
                                    Script.Errors.Add(lineCounter, "Incorrect math expression. " & invalid_msg & " : " & c.Arguments(1))
                                    Continue While
                                End If
                            End If
                        Case Command.Ids.err
                            If c.Arguments.Count = 0 Then
                                Script.Errors.Add(lineCounter, "Incorrect statement: " & line.Trim())
                            Else
                                Script.Errors.Add(lineCounter, c.Arguments(0))
                            End If
                            Continue While
                        Case Command.Ids.call
                            Dim calledScript As Script = FindByName(c.Arguments(0), currentScript)
                            If calledScript Is Nothing Then
                                Script.Errors.Add(lineCounter, "Function not found: " & line.Trim())
                            Else
                                c.Arguments.Clear()
                                c.Arguments.Add(calledScript.ScriptAddress)
                            End If
                    End Select

                    currentScript.AddCommand(c)
                End If
            End While
        End Using
        FromFile.Close()

        Script.Root = RetScript
        Return RetScript
    End Function
    Public Sub New()
        Me.Commands = New List(Of Command)
    End Sub
    Public Sub New(Optional ScriptName As String = "")
        Me.New()
        If ScriptName.Length > 0 Then _
             Me.Name = ScriptName _
        Else Me.Name = Guid.NewGuid().ToString()
    End Sub
    Public Sub New(FromCommands As List(Of Command))
        Me.New()
        For Each c As Command In FromCommands
            Me.AddCommand(c)
        Next
    End Sub
    Public Sub AddCommand(Command As Command)
        If Command.Id = Command.Ids.empty Then Exit Sub
        If Command.Id <> Command.Ids.err Then
            Me.Commands.Add(Command)
        Else
            Dim args$ = String.Empty
            For Each a$ In Command.Arguments : args &= a & ", " : Next
            If args <> String.Empty Then args = args.Substring(0, args.Length - 2)
        End If
    End Sub
    Public Function Execute() As String
        If Script.Errors.Count > 0 Then Return Nothing

        Me.Variables.Clear()

        For Each C As Command In Me
            Select Case C.Id
                Case Command.Ids.variable
                    If Not Me.Variables.ContainsKey(C.Arguments(0)) Then
                        Me.Variables.Add(C.Arguments(0), 0)
                    End If
                    If C.Arguments.Count > 1 Then
                        Try
                            Me.Variables(C.Arguments(0)) = Calculator.Eval(C.Arguments(1), Me.Variables)
                            Form1.TextB.AppendText("calculated:  " & C.Arguments(0) & " = " & Me.Variables(C.Arguments(0)) & Chr(13) & Chr(10))
                        Catch ex As Exception
                            Form1.TextB.AppendText(String.Format("{0}[{1} = {2}]{0}{3}: {4}{0}",
                                                                 Chr(13) & Chr(10),
                                                                 C.Arguments(0), C.Arguments(1),
                                                                 ex.GetType(), ex.Message))
                        End Try
                    End If
                Case Command.Ids.repeat
                    Continue For
                Case Else : C.Execute()
            End Select
        Next

        Return String.Empty
    End Function

    Public Overrides Function Equals(obj As Object) As Boolean
        Return TypeOf obj Is Script And Me.Name = obj.Name
    End Function
    Public Overrides Function ToString() As String
        Dim n$ = "{root}"
        If Me.Name.Length > 0 Then n = "[" & Me.Name & "]"
        Return n & " commands: " & Me.Commands.Count
    End Function
    Public Shared Operator =(a As Script, b As Script)
        Return a.Name = b.Name
    End Operator
    Public Shared Operator <>(a As Script, b As Script)
        Return a.Name <> b.Name
    End Operator

    Public Function GetEnumerator() As IEnumerator(Of Command) Implements IEnumerable(Of Command).GetEnumerator
        Return Me.Commands.GetEnumerator()
    End Function
    Public Function GetEnumerator_() As IEnumerator Implements IEnumerable.GetEnumerator
        Return Me.Commands.GetEnumerator()
    End Function


End Class