﻿Public Class Command

    Public Enum Ids As SByte
        ''' <summary> Indicates error </summary>
        err = -1
        ''' <summary> Means empty command (that skipping) </summary>
        empty
        ''' <summary> Calls a named function: call MyFunc </summary>
        [call]
        ''' <summary> Declares new variable or sets value to existing </summary>
        variable
        ''' <summary> Interrupts a script for a few time: delay 100ms </summary>
        delay
        ''' <summary> Moves mouse cursor in some point: move 123 345 </summary>
        move
        ''' <summary> Mouse left click at some point: click 123 345 </summary>
        click
        ''' <summary> Mouse left click (without args) </summary>
        lclick
        ''' <summary> Mouse right click (without args) </summary>
        rclick
        ''' <summary> Mouse click on a middle button (scroll button) </summary>
        mclick
        ''' <summary> Simulates pressing and releasing of a kbd key: keypress A5 </summary>
        keypress
        ''' <summary> Simulates pressing (not releasing) of a kbd key: keydown A5 </summary>
        keydown
        ''' <summary> Simulates releasing of a kbd key: keyup A5 </summary>
        keyup
        ''' <summary> Scrolls a mouse wheel on a few points: scroll -10 </summary>
        scroll
        ''' <summary> Writes a string by simulation of key presses: write Hello! </summary>
        write
        ''' <summary> Returns mouse pointer at the start position (where script was started) </summary>
        retstart
        ''' <summary> Repeats previous command X times </summary>
        repeat
        ''' <summary> Full stops script execution </summary>
        [stop]

        [if]
        [else]
        [endif]

    End Enum

    Public Shared RequiementsForArgs As New Dictionary(Of Ids, Byte) From {
        {Ids.err, 0},
        {Ids.empty, 0},
        {Ids.move, 2},
        {Ids.click, 2},
        {Ids.lclick, 0},
        {Ids.rclick, 0},
        {Ids.mclick, 0},
        {Ids.retstart, 0},
        {Ids.if, 0},
        {Ids.else, 0},
        {Ids.endif, 0}
    } ' By default, every command should have one argument at least

    Public Shared Aliases As New Dictionary(Of Command.Ids, String) From {
        {Ids.keypress, "keypress kp key"},
        {Ids.keydown, "keydown kd press"},
        {Ids.keyup, "keyup ku release"},
        {Ids.call, "call run start execute"},
        {Ids.delay, "delay wait sleep pause"},
        {Ids.click, "click mouseclick clickat"},
        {Ids.write, "write print type echo"},
        {Ids.variable, "variable var set let dim new"},
        {Ids.repeat, "repeat cycled andmore"},
        {Ids.if, "if"}
    }

    Public Shared KnownKeys As New Dictionary(Of String, Byte) From {
        {"backspace", 8},
        {"tab", 9},
        {"enter", 13},
        {"shift", 16},
        {"ctrl", 17},
        {"alt", 18},
        {"pause", 19},
        {"capslock", 20},
        {"esc", 27},
        {"space", 32},
        {"pageup", 33},
        {"pagedown", 34},
        {"end", 35},
        {"home", 36},
        {"left", 37},
        {"up", 38},
        {"right", 39},
        {"down", 40},
        {"printscreen", 44},
        {"insert", 45},
        {"delete", 46},
        {"windows", 91},
        {"context", 93},
        {"numlock", 144},
        {"scrolllock", 145},
        {"mute", 173},
        {"volumedown", 174},
        {"volumeup", 175},
        {"musicnext", 176},
        {"musicprev", 177},
        {"musicpause", 179},
        {"musicstop", 178}
    }

    Public Property Name As String = "err"
    Public Property Id As Command.Ids
    Public Property Arguments As New List(Of String)
    Public Property ParentScript As Script = Nothing

    Public Sub Execute()
        If Not InArray(Me.Id, {Ids.empty, Ids.err}) Then
            Go.Actions(Me.Name).Invoke(Nothing, Arguments.ToArray)
        End If
    End Sub

    Public Sub New(FromString$)
        Me.Id = Ids.err
        Dim commentPos As Int32
        Dim fromNorm = Norm(FromString)


        If fromNorm.Length = 0 OrElse InArray(fromNorm(0), {"'"c, "#"c}) Then
            Me.Id = Ids.empty
            Exit Sub
        End If

        RemoveDoubleSpaces(fromNorm)

        commentPos = fromNorm.IndexOfAny("'#")
        If commentPos > -1 Then
            fromNorm = fromNorm.Substring(0, commentPos)
        End If

        Dim I As Ids = Ids.err

        For Each Ix As Ids In [Enum].GetValues(GetType(Ids))
            If Ix = Ids.write Or Ix = Ids.variable Or Ix = Ids.if Then Continue For
            If fromNorm.StartsWith(Ix.ToString()) OrElse _
                Aliases.ContainsKey(Ix) AndAlso Aliases(Ix).IndexOf(Norm(fromNorm).Split()(0)) > -1 Then
                I = Ix
                Exit For
            End If
        Next

        If I = Ids.err Then
            If Array.IndexOf(Aliases(Ids.write).Split(), fromNorm.Split()(0)) > -1 Then
                Dim rx$ = String.Format("^\s*(?:" & Aliases(Ids.write).Replace(" ", "|") & ")\s+" & _
                                        "(?:['{0}]([^'{0}]+)['{0}]|(\S+)).*$", Chr(34))
                FromString = RegFindAll(FromString, rx)(0).Trim(Chr(34), "'")
                Me.Name = "write" : Me.Id = Ids.write : Me.Arguments.Add(FromString)
            ElseIf Array.IndexOf(Aliases(Ids.variable).Split(), fromNorm.Split()(0)) > -1 Then
                Dim rx$ = "^\s*(?:" & Aliases(Ids.variable).Replace(" "c, "|") & ")\s+([A-Za-z_][A-Za-z\d_]*)\s*(?:=?\s*(.+)\s*)?$"
                Dim var As String() = RegFindAll(fromNorm, rx)
                If var.Length = 0 Then
                    Me.Arguments = New List(Of String) From {"Incorrect variable-declaration statement: " & FromString.Trim()}
                Else
                    Me.Arguments.Clear()
                    Me.Arguments.AddRange(var)
                    Me.Id = Ids.variable
                End If
            End If
            Exit Sub
        End If

        Me.Name = I.ToString()
        Me.Id = I

        ' If args arent requied
        If RequiementsForArgs.ContainsKey(Me.Id) AndAlso RequiementsForArgs(Me.Id) = 0 Then Exit Sub

        Dim args As String() = fromNorm.Split(),
             arg As String

        For x% = 1 To args.Length - 1 Step 1
            arg = args(x)
            If I.ToString().StartsWith("key") Then
                    If Not IsNumeric(arg) AndAlso KnownKeys.ContainsKey(arg) Then
                        Dim b As Byte
                        KnownKeys.TryGetValue(arg, b)
                        arg = b.ToString()
                    End If
                    Dim R% = 0
                If Not Integer.TryParse(arg, R) _
                   OrElse CInt(arg) > 255 OrElse CInt(arg) <= 0 Then
                    If Me.Arguments.Count > 0 Then Exit For
                    Me.Arguments.Clear()
                    Me.Arguments.Add("Incorrect key-code in statement: " & fromNorm)
                    Me.Id = Ids.err
                    Exit Sub
                End If
            ElseIf I.ToString() = "if" Then
                Dim expression = fromNorm.Substring(fromNorm.IndexOf(" ") + 1)
                Me.Arguments.Clear()
                Me.Arguments.Add(expression)
                Exit For
            End If

            Me.Arguments.Add(arg)
        Next

        Dim reqArgs As Byte = 1
        If RequiementsForArgs.ContainsKey(Me.Id) Then
            reqArgs = RequiementsForArgs.Item(Me.Id)
        End If
        If reqArgs > Me.Arguments.Count Then
            Me.Id = Ids.err
            Me.Arguments.Clear()
            Me.Arguments.Add(String.Format("Command '{0}' should have {1} arguments at least", I.ToString(), reqArgs))
        End If

    End Sub

    Public Overrides Function ToString() As String
        Dim args$ = String.Empty
        For Each Ar$ In Arguments
            args &= "'" & Ar & "', "
        Next
        args = args.TrimEnd(" "c, ","c)
        Return Me.Id.ToString() & " (" & args & ")"
    End Function

End Class